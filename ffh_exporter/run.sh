#!/bin/bash
#set -v
#set -e
set -x

python history_exporter.py -h
python history_exporter.py --version
NOW=$(date +"%Y-%m-%d_%H.%M.%S")
time ( python history_exporter.py --force --csv-report FULL -o output_${NOW}.out --debug=DEBUG )

