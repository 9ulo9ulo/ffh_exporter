#!/usr/bin/env python3
# -*- coding: utf-8 -*-


__prog__ = 'history_exporter.py'
__version__ = '0.4.1'
__author__ = '9ulo9ulo'


import argparse
import glob
import logging
import os
import re
import sqlite3
import sys
import time


class HistoryExporter(object):


    log_levels = {'WARNING': logging.WARNING, 'INFO': logging.INFO, 'DEBUG': logging.DEBUG, 'NOTSET': logging.NOTSET}


    def __init__(self, **kwargs):
        self.sqlite3db_file = kwargs['sqlite3db_file']
        self.out_file = kwargs['out_file']
        self.time_stamp = kwargs['time_stamp']
        self.SHOW_RESULTS = kwargs['show_results']
        self.csv_report = kwargs['csv_report']
        self.OVERWRITE_OUTPUT = kwargs['overwrite_output']
        self.DEBUG_LEVEL = kwargs['debug_level']
        self._check_params()
        self.results = None


    def _check_params(self):
        '''
        this method checks all script input parameters which are mainly
        fetched in __init__ method. there are some basic checks performed
        here: time stamp is handled and matched to regex (re_time_stamp_str)
        input sqlite3 database file is being checked (based on detected
        operating system) and at the end output file existence is tested as
        well
        '''
        logging.basicConfig(format='%(asctime)s %(levelname)s %(message)s',
                            datefmt='%Y-%m-%d %H:%M:%S',
                            level=self.log_levels[self.DEBUG_LEVEL])
        logging.debug('sqlite3db_file = %s', self.sqlite3db_file)
        logging.debug('out_file = %s', self.out_file)
        logging.debug('time_stamp = %s', self.time_stamp)
        logging.debug('SHOW_RESULTS = %d', self.SHOW_RESULTS)
        logging.debug('REPORT = %s', self.csv_report)
        logging.debug('OVERWRITE_OUTPUT = %d', self.OVERWRITE_OUTPUT)
        logging.debug('DEBUG_LEVEL = %s', self.DEBUG_LEVEL)
        #handling time stamp
        if not self.time_stamp:
            self.time_stamp = time.strftime('%Y-%m-%d', time.localtime())
        else:
            re_time_stamp_str = '^20(0\d|1[01234])-(0[1-9]|1[012])-(0[1-9]|[12][0-9]|3[01])$'
            regex = re.compile(re_time_stamp_str)
            r = regex.match(self.time_stamp)
            if not r:
                logging.error('error! could not match time stamp: %s (regex: %s)', self.time_stamp, re_time_stamp_str)
                sys.exit(1)
        logging.info('time_stamp = %s', self.time_stamp)
        #checking input file
        if self.sqlite3db_file:
            if not os.path.exists(self.sqlite3db_file):
                logging.error('error! could not find input file: %s', self.sqlite3db_file)
                sys.exit(1)
        else:
            if os.name == 'posix':
                logging.debug('posix system detected')
                HOME_DIR = os.path.expanduser('~')
                logging.debug('HOME_DIR = %s', HOME_DIR)
                self.sqlite3db_file = glob.glob(os.path.join(HOME_DIR, '.mozilla', 'firefox', '*.default', 'places.sqlite'))
            else:
                logging.debug('non posix system detected')
                APPDATA_DIR = os.path.expandvars('%APPDATA%')
                logging.debug('APPDATA_DIR =%s', APPDATA_DIR)
                self.sqlite3db_file = glob.glob(os.path.join(APPDATA_DIR, 'Mozilla', 'Firefox', 'Profiles', '*.default', 'places.sqlite'))
            logging.debug('trying to load places.sqlite file from default dir')
            if len(self.sqlite3db_file) == 1:
                self.sqlite3db_file = self.sqlite3db_file[0]
            else:
                logging.warning('warning! could not automagically fetch places.sqlite file')
                logging.warning('warning! please use explicitly -i/--input-file option')
                sys.exit(1)
        logging.info('sqlite3db_file = %s', self.sqlite3db_file)
        #checking output file
        if os.path.exists(self.out_file) and self.OVERWRITE_OUTPUT:
            logging.warning('warning! overwriting existing file: %s', self.out_file)
        elif os.path.exists(self.out_file) and not self.OVERWRITE_OUTPUT:
            logging.error('error! output file %s already exists', self.out_file)
            sys.exit(1)
        else:
            logging.info('result data will be stored in file: %s', self.out_file)


    def _load(self):
        '''
        this method connects to default firefox "places.sqlite" file where
        entire browsing history is stored. then extracts all entries (visited
        www page list) according to self.time_stamp variable value. there
        is also a smart time conversion implemented here. thanks to that
        some duplicated entries are skipped and not visible in final results.
        it was notified that firefox can send several request to single www
        page during one second which is not fully usable for our purposes.
        due to that fact time conversion was applied and final results are
        smaller, they also don't contain duplicated entries at all
        '''
        logging.debug('extracting data from file: %s', self.sqlite3db_file)
        db_connection = sqlite3.connect(self.sqlite3db_file)
        db_cursor = db_connection.cursor()
        sql_query_str = 'SELECT visit_date, title, url FROM moz_historyvisits AS H JOIN moz_places AS P ON H.place_id=P.id;'
        logging.debug('sql_query_str = "%s"', sql_query_str)
        db_cursor.execute(sql_query_str)
        self.results = db_cursor.fetchall()
        db_cursor.close()
        db_connection.close()
        len1 = len(self.results)
        logging.debug('total fetched entries (before time conversion): %d', len1)
        TIME_FACTOR = 0.000001   #wtf? -> http://stackoverflow.com/questions/12400256/python-converting-epoch-time-into-the-datetime?answertab=votes#tab-top
        #time conversion & removing duplicated lines via set datatype
        self.results = list(set([(time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(i[0] * TIME_FACTOR)), i[1], i[2]) for i in self.results]))
        self.results.sort(key=lambda x: x[0])
        len2 = len(self.results)
        logging.debug('total fetched entries (after time conversion and duplicated lines removal): %d', len2)
        logging.debug('total removed duplicates: %d', len1 - len2)


    def _filter_results_by_date(self):
        '''
        this method filters all data fetched in self.results list according
        to value of self.time_stamp variable
        '''
        self.results = [i for i in self.results if i[0].startswith(self.time_stamp)]


    def _save_csv_hourly_report_for_last_day(self):
        '''
        this method produces hourly report for data fetched during last day.
        the report can be generated in two formats: FULL for the whole day
        (24hours: from 00:00:00 to 23:59:59) and BRIEF for those hours
        only where real non-zero data was recorded. according to debug level
        additional data can be printed on the console. this method will also
        store fetched data in output *.csv file named according to variable
        self.out_file with additional ".csv" extension.
        '''
        hour_dict = {}
        if self.csv_report == 'FULL':
             for i in range(24):
                 hour_dict[i] = 0
        for i in self.results:
            extracted_hour = int(i[0].split()[-1].split(':')[0])
            if extracted_hour in hour_dict.keys():
                hour_dict[extracted_hour] += 1
            else:
                hour_dict[extracted_hour] = 1
        logging.debug('hour_dict = %s', str(hour_dict))
        hour_dict_keys = sorted(hour_dict.keys())
        logging.debug('hour_dict_keys = %s', hour_dict_keys)
        print('hourly report for: {}'.format(self.time_stamp))
        for k in hour_dict_keys:
            print('{:>2} -> {}'.format(k, hour_dict[k]))
        print('total entries: {}'.format(sum(hour_dict.values())))
        csv_header  = ','.join('h'+str(k).zfill(2) for k in hour_dict_keys)
        csv_line  = ','.join(str(hour_dict[k]) for k in hour_dict_keys)
        logging.debug('csv_hdeader = {}'.format(csv_header))
        logging.debug('csv_line = {}'.format(csv_line))
        with open(self.out_file+'.csv', 'w', encoding='utf-8') as f:
            f.write(csv_header + os.linesep)
            f.write(csv_line + os.linesep)
            logging.info('saved report results in %s file', self.out_file+'.csv')


    def _save_results_to_out_file(self):
        '''
        this method will save all results (visited www pages) fetched in
        self.results list to textual file specified by self.out_file
        variable
        '''
        logging.debug('saving extracted data to output file: %s', self.out_file)
        with open(self.out_file, 'w', encoding='utf-8') as f:
            for i in self.results:
                f.write(str(i) + os.linesep)
            logging.info('saved %d lines in %s file', len(self.results), self.out_file)


    def show_results(self):
        '''
        this method will display on console all results (visited www pages)
        fetched in self.results list
        '''
        print('fetched results:')
        for i in self.results:
            print(i)
        print('total: {}'.format(len(self.results)))


    def extract_data(self):
        '''
        this is only a wrapper method for other methods from HistoryExporter
        class
        '''
        self._load()
        self._filter_results_by_date()
        self._save_results_to_out_file()
        if self.csv_report:
            self._save_csv_hourly_report_for_last_day()
        if self.SHOW_RESULTS:
            self.show_results()
        logging.debug('done')




#main function
def main(cli_args_dict):
    '''
    main function of history_exporter.py module
    '''
    hi_ex = HistoryExporter(**cli_args_dict)
    hi_ex.extract_data()




#call context
if __name__ == '__main__':
    cli_parser = argparse.ArgumentParser(prog=__prog__,
                                         description='extract browsing history from firefox browser (places.sqlite file)')
    cli_parser.add_argument('-i',
                            '--input-file',
                            dest='sqlite3db_file',
                            metavar='SQLITE3_DATABASE_FILE',
                            default='',
                            help='''absolute path to places.sqlite database file. if empty (default) script will automagically
                                try to fetch places.sqlite file from default firefox directory''')
    cli_parser.add_argument('-o',
                            '--output-file',
                            dest='out_file',
                            metavar='OUT_FILE',
                            default='output.out',
                            help='output *.out file (default: %(default)s)')
    cli_parser.add_argument('-t',
                            '--timestamp',
                            dest='time_stamp',
                            metavar='YYYY-MM-DD',
                            default='',
                            help='time stamp for data filtering. if empty (default) only data for present day will be fetched')
    cli_parser.add_argument('-s',
                            '--show',
                            dest='show_results',
                            action='store_true',
                            help='print fetched results (default: %(default)s)')
    cli_parser.add_argument('--csv-report',
                            dest='csv_report',
                            choices=['BRIEF', 'FULL'],
                            help='generate (one of two kinds) hourly report for last day (default: %(default)s) then save it in *.csv file')
    cli_parser.add_argument('--force',
                            dest='overwrite_output',
                            action='store_true',
                            help='overwrite destination *.out file if exists (default: %(default)s')
    cli_parser.add_argument('-d',
                            '--debug',
                            dest='debug_level',
                            choices=['DEBUG', 'INFO', 'WARNING', 'NOTSET'],
                            default='WARNING',
                            help='increasing verbosity level (default: %(default)s)')
    cli_parser.add_argument('--version',
                            action='version',
                            version='%(prog)s '+__version__)
    cli_args = cli_parser.parse_args()
    main(cli_args_dict=vars(cli_args))




#70D0:
# - some docstrings are missing
# - change some print statements to logging functions
# - detailed script description file is needed

